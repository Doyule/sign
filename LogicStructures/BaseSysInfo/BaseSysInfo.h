#ifndef BASESYSINFO_H
#define BASESYSINFO_H

struct BaseSysInfo{
public:
    BaseSysInfo() {
        fileName = "C:\\SIGNER.txt",  // Full Name to DATEBASE file with shapes info
                                      // ( must be created before starting app )
        sep = "###",
        caption = "Sign<Engine>",     // GLUT window caption
        WIN_SIZE = 600,               // GLUT window size
        LOGIC_MODE = 1;

        backColor = new float[3];     // Background color
        backColor[0] = 1.0;
        backColor[1] = 1.0;
        backColor[2] = 1.0;

        drawColor = new float[3];     // Draw color
        drawColor[0] = 0.0;
        drawColor[1] = 0.0;
        drawColor[2] = 0.0;

        };

    double WIN_SIZE;
    int LOGIC_MODE;
    std::string fileName;
    std::string sep;
    std::string caption;
    float* backColor;
    float* drawColor;
};

#endif // BASESYSINFO_H
