#include "../Exception/Exception.h"
#include "../BaseSysInfo/BaseSysInfo.h"
#include "../math/math.h"
#include "Recognizer.h"
#include <iostream>
#include <fstream>
#include <math.h>
using namespace std;

const int MAX_ANGLE_DIFF = 18.0;
const double MAX_ATTITUDE_DIFF = 0.15;
const double MIN_PER_RECOGNITION = 70.0;

void Recognizer::ShowRecognition()
{
    cout<<"---It is '"<<single->name<<"' ("<<single->percent<<"%)"<<" !---"<<endl;
    single->clear();
}

void Recognizer::LoadShapeBase()
{
    BaseSysInfo *info = new BaseSysInfo();
    LoadFigure *load = new LoadFigure();
    ifstream f(info->fileName.c_str());

    if(!f) throw Exception("FailBit is setted. Must be created a file named '"+info->fileName+"'");

    string read;
    double d;
    bool is_name = false;
    while(f>>read)
    {
        if(read=="" || read=="\n" || read=="\r" || read==" ") continue;
        if(read == info->sep) { if(load->name!="") load_shapes.push_back(*load); load->clear(); is_name=true; continue;}
        if(is_name==true) {is_name=false; load->name=read; continue;}
        d=atof(read.c_str());
        if(floor(d)==d) load->angles.push_back(d); else load->side_sz.push_back(d);
    }
    if(load->name!="") load_shapes.push_back(*load);

    f.close();
}

void Recognizer::Recognize(LogicFigure* logic)
{
    vector<RecogInfo> supposed;
    vector<RecogInfo> supposed_real;
    for(int j=0;j<load_shapes.size();j++)
    {
        LoadFigure *load = &load_shapes[j];
        int num = 0;
        int p1 = logic->vertex.size()-1;
        int p2 = 0;
        int per = 0;
        for(int i=1;i<logic->vertex.size();i++)
        {
            int angle = countAngle( logic->vertex[p1], logic->vertex[p2], logic->vertex[i] );
            if( _abs(load->angles[num]-angle)<=MAX_ANGLE_DIFF ){
                    double att = (double)getDistance( logic->vertex[i], logic->vertex[p2] )/getDistance( logic->vertex[p2],
logic->vertex[p1] );
                    double att_diff = _abs(load->side_sz[num]-att);
                    if( att_diff<=MAX_ATTITUDE_DIFF ) { per++; p1=p2; p2=i; num++; /*cout <<"* "<< att <<"-"<< load->side_sz[num] << endl;*/ }
            }
        }
        supposed.push_back(RecogInfo(load->name,(double)per/load->angles.size()*100.0,load->angles.size()));
    }

    supposed_real.clear();
    for(int i=0;i<supposed.size();i++)
        if(supposed[i].percent>=MIN_PER_RECOGNITION) supposed_real.push_back(supposed[i]);

    for(vector<RecogInfo>::iterator itr=supposed_real.begin();itr!=supposed_real.end();++itr)
    if( itr->angle_sz>=single->angle_sz )
        { single->percent = itr->percent; single->name=itr->name; single->angle_sz=itr->angle_sz;}

        cout<<" ################################# "<<endl;
        for(vector<RecogInfo>::iterator itr=supposed.begin();itr!=supposed.end();++itr)
        cout<<itr->name<<" - "<<itr->percent<<"%"<<endl;

}
