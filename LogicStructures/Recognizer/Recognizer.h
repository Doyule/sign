#ifndef RECOGNIZER_H
#define RECOGNIZER_H
#include "../Figure/Figure.h"

struct RecogInfo{
public:
    RecogInfo(){ this->clear(); };
    RecogInfo(string _name, double _p, int _sz) :
        name(_name), percent(_p), angle_sz(_sz) {};
    void clear(){ name=""; angle_sz=0; percent=0; }

    string name;
    double percent;
    int angle_sz;
};

// ###########################################################################################

class Recognizer{
public:
    Recognizer() : single(new RecogInfo()) {};
    void Recognize(LogicFigure*);
    void LoadShapeBase();
    void ShowRecognition();

    RecogInfo *single;
    vector<LoadFigure> load_shapes;

};


#endif // RECOGNIZER_H_INCLUDED
